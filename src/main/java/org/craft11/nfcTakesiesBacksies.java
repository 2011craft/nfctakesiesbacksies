package org.craft11;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class nfcTakesiesBacksies extends JavaPlugin {

    private static Material cobblestoneMaterial = Material.getMaterial(4);
    private static ItemStack cobblestoneStack = new ItemStack(cobblestoneMaterial, 9);

    private static Material stageOneMaterial = Material.getMaterial(684);
    private static ItemStack stageOneStack = new ItemStack(stageOneMaterial, 9);

    private static Material stageTwoMaterial = Material.getMaterial(685);
    private static ItemStack stageTwoStack = new ItemStack(stageTwoMaterial, 9);

    private static Material stageThreeMaterial = Material.getMaterial(686);


    @Override
    public void onDisable() {
        this.getServer().getScheduler().cancelTasks(this);
    }

    @Override
    public void onEnable() {
        PluginManager pm = this.getServer().getPluginManager();
        ShapelessRecipe backToCobblestone = new ShapelessRecipe(cobblestoneStack);
        ShapelessRecipe backToStageA = new ShapelessRecipe(stageOneStack);
        ShapelessRecipe backToStageB = new ShapelessRecipe(stageTwoStack);

        backToCobblestone.addIngredient(stageOneMaterial);
        backToStageA.addIngredient(stageTwoMaterial);
        backToStageB.addIngredient(stageThreeMaterial);

        getServer().addRecipe(backToCobblestone);
        getServer().addRecipe(backToStageA);
        getServer().addRecipe(backToStageB);
    }
}
